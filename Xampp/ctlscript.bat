@echo off
rem START or STOP Services
rem ----------------------------------
rem Check if argument is STOP or START

if not ""%1"" == ""START"" goto stop

if exist D:\Programas\Xampp\hypersonic\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\server\hsql-sample-database\scripts\ctl.bat START)
if exist D:\Programas\Xampp\ingres\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\ingres\scripts\ctl.bat START)
if exist D:\Programas\Xampp\mysql\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\mysql\scripts\ctl.bat START)
if exist D:\Programas\Xampp\postgresql\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\postgresql\scripts\ctl.bat START)
if exist D:\Programas\Xampp\apache\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\apache\scripts\ctl.bat START)
if exist D:\Programas\Xampp\openoffice\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\openoffice\scripts\ctl.bat START)
if exist D:\Programas\Xampp\apache-tomcat\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\apache-tomcat\scripts\ctl.bat START)
if exist D:\Programas\Xampp\resin\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\resin\scripts\ctl.bat START)
if exist D:\Programas\Xampp\jboss\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\jboss\scripts\ctl.bat START)
if exist D:\Programas\Xampp\jetty\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\jetty\scripts\ctl.bat START)
if exist D:\Programas\Xampp\subversion\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\subversion\scripts\ctl.bat START)
rem RUBY_APPLICATION_START
if exist D:\Programas\Xampp\lucene\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\lucene\scripts\ctl.bat START)
if exist D:\Programas\Xampp\third_application\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\third_application\scripts\ctl.bat START)
goto end

:stop
echo "Stopping services ..."
if exist D:\Programas\Xampp\third_application\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\third_application\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\lucene\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\lucene\scripts\ctl.bat STOP)
rem RUBY_APPLICATION_STOP
if exist D:\Programas\Xampp\subversion\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\subversion\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\jetty\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\jetty\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\hypersonic\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\server\hsql-sample-database\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\jboss\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\jboss\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\resin\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\resin\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\apache-tomcat\scripts\ctl.bat (start /MIN /B /WAIT D:\Programas\Xampp\apache-tomcat\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\openoffice\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\openoffice\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\apache\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\apache\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\ingres\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\ingres\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\mysql\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\mysql\scripts\ctl.bat STOP)
if exist D:\Programas\Xampp\postgresql\scripts\ctl.bat (start /MIN /B D:\Programas\Xampp\postgresql\scripts\ctl.bat STOP)

:end

